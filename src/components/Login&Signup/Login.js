import './loginSignup.css'
import Header from '../toDO/header'

import axios from 'axios'
import { useDispatch } from 'react-redux'
import { logIn } from '../../redux/authSlice'

import { toast } from 'react-toastify';

function Login() {

  // This login is just for checking if user on login page or signup
    const loginchecker = true


  // Main logic
  const dispatch = useDispatch()


  const loginSubmitted = async (e) => {

    e.preventDefault()

    const formData  = new FormData(e.target);
    const Email     = formData.get('Email')
    const Password  = formData.get('Password')


      try {

        const response = await axios.get('http://localhost:6942/LoginChecker');
        
        if (response.data.find((f) => f.Email === Email && f.Password === Password)) {

          const user = response.data.find((f) => f.Email === Email && f.Password === Password)
          toast.success('Connected!');
          dispatch(logIn(user._id));
          window.location.assign("http://localhost:3000/list")

        } 

        else if (response.data.find((f) => f.Email !== Email)) {
          toast.warn('Email incorrect!');
        } 

        else{
          toast.warn('Password incorrect!');
        }

      } catch (err) {
        console.log(err);
      }

  }


    return (
        <>  
        <Header login={loginchecker}/>
          <div className='centerBody'>
            <div className="center">
                <h1>login</h1>

                <form onSubmit={loginSubmitted}>
                    <div className="inputbox">
                      <input type="text" name='Email' required="required"></input>
                      <span>Email</span>
                    </div>
                    <div className="inputbox">
                      <input type="text" name='Password' required="required"></input>
                      <span>Password</span>
                    </div>
                    <div className="inputbox">
                      <input type="submit" value={'Login'}/>
                    </div>
                </form>

            </div>
          </div>
        </>
    )
}
export default Login
