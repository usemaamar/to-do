import './loginSignup.css'
import Header from '../toDO/header'

import axios from 'axios';
import { logIn } from '../../redux/authSlice'
import { useDispatch } from 'react-redux';


import { toast } from 'react-toastify';


const Signup = () => {

const dispatch = useDispatch()
  const signupSubmitted = async (e) => {
    e.preventDefault()
  
    const formData  = new FormData(e.target);
  
    const UserName      = formData.get('UserName')
    const Email         = formData.get('Email')
    const Password      = formData.get('Password')
    const chkPassword   = formData.get('chkPassword')
    const da = {UserName, Email, Password}


    try{
      
      const response = await axios.get('http://localhost:6942/LoginChecker');
      if (response.data.length > 0) {
        if (response.data.find((f) => f.Email !== Email && f.UserName !== UserName) && Password === chkPassword ) {
          const response = await axios.post('http://localhost:6942/SignupChecker', da)
          dispatch(logIn(response.data))
          toast.success('Connected!')
          window.location.assign("http://localhost:3000/list")
        }
  
        if(response.data.find((f) => f.Email === Email)){
          toast.warn('Email already in use!')
        }
        if ((response.data.find((f) => f.UserName === UserName)) ) {
          toast.warn('UseName already in use!')
        }
        if(Password !== chkPassword){
          toast.warn('Password doesn\'t match!')
        }
      }
      else{
        const response = await axios.post('http://localhost:6942/SignupChecker', da)
          dispatch(logIn(response.data))
          toast.success('Connected!')
          window.location.assign("http://localhost:3000/list")

      }

    }
    catch(err){
      console.log(err);
    }

  }



    return (
        <>
        <Header />

          <div className='centerBody'>
            <div className="center">
              <h1>Signup</h1>
              <form onSubmit={signupSubmitted}>
              <div className="inputbox">
                  <input type="text" name='UserName' required="required" maxLength={50}></input>
                  <span>username</span>
                </div>
                <div className="inputbox">
                  <input type="text" name='Email' required="required"></input>
                  <span>Email</span>
                </div>
                <div className="inputbox">
                  <input type="text" name='Password' required="required"></input>
                  <span>Password</span>
                </div>
                <div className="inputbox">
                  <input type="text" name='chkPassword' required="required"></input>
                  <span>confirmer password</span>
                </div>
                <div className="inputbox">
                  <input type="submit" value={'Signup'}/>
                </div>
              </form>
            </div>
          </div>
        </>
    )
}

export default Signup