import { useState, useRef, useEffect } from "react"
import axios from "axios";
import { useSelector } from "react-redux";

import Task from "./Task";
import TaskCompleted from "./TaskCompleted";
import ListSlide from "./listSlide";


import { SlArrowUp } from "react-icons/sl";

export default function List() {

    const globalId    = useSelector(state => state.auth.userID)


    const [list, setList] = useState([])

    const dropDown       = useRef()
    const dropDownButton = useRef()
    const slide          = useRef()
    

    useEffect(() => {
        const fetchData = async () => {
            try{
                const result = await axios.get(`http://localhost:6942/TaskList/${globalId}`)
                setList(result.data)

                if (result.data.length === 0) {
                    dropDown.current.classList.add('dropDown-toggle')
                    setTimeout(() => {
                        dropDown.current.style.zIndex = '1'
                    }, 200);
                }
            }
            catch(err){
                console.log(err);
            }
        }
        fetchData()
    }, [globalId])



    const dropDownHandler = (e) => {

       setTimeout(() => {
        if (dropDown.current.classList.contains('dropDown-toggle')) {

            dropDown.current.style.zIndex = ''
            setTimeout(() => {
                dropDown.current.classList.remove('dropDown-toggle')
            }, 200);
            setTimeout(() => {
                dropDown.current.firstChild.value = ''
                setTaskValue(0)
            }, 400);
        }

        else{
            dropDown.current.classList.add('dropDown-toggle')
            setTimeout(() => {
                dropDown.current.style.zIndex = '1'
            }, 200);

        }
        dropDownButton.current.classList.toggle('taskHolder-btn-toggle')
       }, 100);
    }




    // For adding tasks-------------------------------------------------

    const date = new Date()
    const daten = date.toLocaleDateString("en-US")

    const [taskValue, setTaskValue] = useState(0)

    const AddTask = async () => {

        
        try{
            if (dropDown.current.firstChild.value !== '') {

                await axios.post(`http://localhost:6942/AddTask`, {daten, taskValue, UserID: globalId, completed: false})
    
                const result = await axios.get(`http://localhost:6942/TaskList/${globalId}`)
                setList(result.data)

                dropDownButton.current.classList.add('taskHolder-btn-toggle')
    
                dropDown.current.style.zIndex = ''
                setTimeout(() => {
                    dropDown.current.classList.remove('dropDown-toggle')
                }, 200);
                setTimeout(() => {
                    dropDown.current.firstChild.value = ''
                    setTaskValue(0)
                }, 400);
            }
        }
        catch(err){
            console.log(err);
        }
    }

    // Exchanging tasks

    const onTaskRemove = async (e) => {
        
        try{
            await axios.patch(`http://localhost:6942/TaskToggle/${e._id}`, {completed: e.completed})
            const result = await axios.get(`http://localhost:6942/TaskList/${globalId}`)
            setList(result.data)

        }
        catch(err){
            console.log(err);
        }
    }


    // opening slide
    const [transmeter, setTransmeter] = useState()

    const slideOpener = (e) => {
        setTimeout(() => {
            slide.current.classList.add('taskSettings-toggle');
            setTransmeter(e); 
            }, 100);
        };

    const closeSide = () => {
        setTimeout(() => {
            slide.current.classList.remove('taskSettings-toggle')
            setTransmeter('')
        }, 50);
    }


    // Task Modification
    const ModifyTask = async (e, i) => {

        try{
            await axios.patch(`http://localhost:6942/ModifyTask/${i}`, {taskValue: e})
            const result = await axios.get(`http://localhost:6942/TaskList/${globalId}`)
            setList(result.data)
        }
        catch(err){
            console.log(err);
        }

    }

    // Task Deletion
    const DeleteTask = async (i) => {
        try{
            await axios.delete(`http://localhost:6942/DelteTask/${i}`)
            const result = await axios.get(`http://localhost:6942/TaskList/${globalId}`)
            setList(result.data)
        }
        catch(err){
            console.log(err);
        }

        setTimeout(() => {
            slide.current.classList.remove('taskSettings-toggle')
            setTransmeter('')
        }, 50);

    }

  return (
   <div className="List" >

        <div ref={dropDown} className="dropDown">
            <input type="text" placeholder="Add a task" maxLength={70} onChange={(e) => {
                  const inputValue = e.target.value;
                  if (inputValue.length > 70) {
                    e.target.value = inputValue.slice(0, 70);
                  }
                  setTaskValue(e.target.value);
                }}/>
            <div> 
                <span>{taskValue.length ? taskValue.length : 0} / 70</span>
                <button onClick={AddTask}>Add</button>
            </div>
        </div>

            <div className='taskHolder'>
                <button onClick={dropDownHandler} ref={dropDownButton} ><SlArrowUp /></button>
                {list.length > 0 ? 
                    <>
                        <Task 
                            list = {list} 
                            onTaskRemove = {onTaskRemove} 
                            slideOpener  = {slideOpener}
                            />
                        <TaskCompleted 
                            list = {list} 
                            onTaskRemove  = {onTaskRemove} 
                            slideOpener   = {slideOpener}
                            />
                    </>
                    : <h1 className="msg">Add a task!</h1>
                }

            </div>
            <ListSlide 
                slide={slide} 
                closeSide={closeSide} 
                transmeter={transmeter}
                ModifyTask={ModifyTask}
                DeleteTask={DeleteTask}
                />
   </div>
  )
}
