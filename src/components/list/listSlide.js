import { useEffect, useState } from "react";
import { BsFillArrowRightCircleFill } from "react-icons/bs";

export default function ListSlide(props) {

  const [textareaValue, setTextareaValue] = useState();
  
  useEffect(() => {
    if(props.transmeter){
      setTextareaValue(props.transmeter.taskValue ? props.transmeter.taskValue : "")
    }
  },[props.transmeter])

  const handleTextareaChange = (e) => {
    const textValidation = e.target.value
    if (textValidation.length > 70) {
      e.target.value = textValidation.slice(0, 70)
    }
    setTextareaValue(e.target.value);
  };

  return (
    <div ref={props.slide} className="taskSettings">
      <BsFillArrowRightCircleFill className="exitBTN" onClick={props.closeSide} />
      {props.transmeter && (
        <div>
          <textarea spellCheck={false} maxLength={70} value={textareaValue} onChange={handleTextareaChange}></textarea>
          <div>
            <button onClick={() => props.ModifyTask(textareaValue, props.transmeter._id)}>Modify</button>
            <button onClick={() => props.DeleteTask(props.transmeter._id)}>Delete</button>
          </div>
        </div>
      )}
    </div>
  );
}
