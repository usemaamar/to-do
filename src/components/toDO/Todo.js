import Header from './header'
import Section from './section'
import './Todo.css'



export default function Todo() {
  return (
    <>
        <Header />
        <Section />
    </>
  )
}
