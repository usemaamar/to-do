import logo from '../../onlyImages/LG.svg';

import { Link } from 'react-router-dom';

export default function Section() {
  return (
    <div className='TodoSection'>
      <img src={logo} alt="" />
      <h1>To Do List</h1>
      <p>The best way to get something done is to begin.</p>

      <Link to="/Signup">Get Started</Link>
    </div>
  );
}
