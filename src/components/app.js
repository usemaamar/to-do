import React from 'react'
import Todo from './toDO/Todo'
import ToDoList from './list/ToDoTracker'
import Login from './Login&Signup/Login'
import Signup from './Login&Signup/Signup'
import NotFound from './notFound'

import { useSelector } from 'react-redux'
import { ToastContainer } from 'react-toastify';
import {BrowserRouter, Routes, Route} from 'react-router-dom'

export default function App() {

  const globalState = useSelector(state => state.auth.isLogged)

  return (
    <>
      <BrowserRouter>
        <Routes>
            {/* must add some kind of condition start*/}

            {!globalState && 
            <>
                <Route path={'/'}         element={<Todo />}/>
                <Route path={'/Login'}    element={<Login />}/>
                <Route path={'/Signup'}   element={<Signup />}/>
            </>}
            {globalState  && <Route path={'/List'}     element={<ToDoList />}/>}
            
            {/* must add some kind of condition end  */}

            

            <Route path={'/*'}        element={<NotFound />}/>
        </Routes>
      </BrowserRouter>

      <ToastContainer
        position="top-right"
        autoClose={2000}
        limit={1}
        hideProgressBar
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
        />
    </>
  )
}
